import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.css']
})
export class DatabindingComponent implements OnInit {

  username = '';

  constructor() { }

  ngOnInit() {
  }

  onClearClick() {
    this.username = '';
  }

  isEmptyUsername() {
    return this.username !== '';
  }
}
